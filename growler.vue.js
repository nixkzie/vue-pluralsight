Vue.config.keyCodes = {
    f1: 112
};

var growler = new Vue({
    el: "#growler",
    data: {
        appName: "Growler",
        query: "",
        isPowerSyntaxEnabled: false,
        searchArea: [],
        selectedSearchIndexes: [
            "beers",
            "breweries"
        ]
    },
    methods: {
        executeSearch: function(event) {
            alert("You searched for " + this.query + " using button " + event.target.innerText);
        },
        parentSearch: function() {
            alert("I go first");
        },
        openInfo: function() {
            alert("Help is displayed");
        }
    }
});